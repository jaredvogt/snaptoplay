
var fs = require("fs");

p = console.log;

///herein lies a whole bunch of utilties to support the game

/**
  * run this for my extensions to base classes
*/
function myStuff() {

    //extend the sting prototype to support format (don't know if this will work in dif file)
    String.prototype.format = function() {
      var formatted = this;
      for (arg in arguments) {
        formatted = formatted.replace("{" + arg + "}", arguments[arg]);
      }
      return formatted;
    };
}

/**
  * log to local file
*/
function logIt(log_message) {
  fs.appendFile('./sms.log', JSON.stringify(log_message) + '\n', function (err) {
        if (err) throw err;
        p('It\'s saved!');
      });
}

exports.logIt = logIt;
exports.myStuff = myStuff;