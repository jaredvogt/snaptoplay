var fs = require("fs"),
    _ = require("underscore"),
    twil = require("./twilioSMS"),
    XRegExp = require('xregexp').XRegExp,
    gserv = require("./gserv"),
    utils = require('./utils'),
    configGameMasterIds = require('./.config').gameMasterIds;

utils.myStuff();  // initialize myStuff
p = console.log;

// Load up the acceptable gameMasters
var gameMasterIds = configGameMasterIds;

/**
  * my two callbacks that get used for message (is there a better way to do this?)
*/
var messageCallback = function (response) {
  p(response);
  utils.logIt(response);  // log outbound information (this logs the time its sent - it does NOT check to see when twilio sent it - need to build)
};  // this is for sendSms so that it gets response from twil before logging

// call back to send sms back in repsonse to incoming message
var senderCallback = function (teamId, msg) {
  twil.sendSms(teamId, msg, messageCallback);
};

/**
  * route message based on teamId
*/
function twilioProcess(teamId, messageIn, sidInbound, sidAccount) {
  p("about to parse");

  // dump transaction to screen
  p("parsing done");
  p('Message from: {0}'.format(teamId));
  p('Message body: {0}'.format(messageIn));
  p('sid: {0}'.format(sidInbound));

  // log inbound message to file
  utils.logIt({teamId: teamId, message: messageIn, sidInbound: sidInbound, date: new Date(), epoch: +new Date()});

  // route by teamId
  // game master messages
  if (game_master_ids.indexOf(teamId) > -1) {  //checks to see if teamId is in game_master_ids - if it isn't, it will return -1
    if (messageIn == "killgames") {  // this is a special message that kills "games" - implemented so that test can run in a loop. Longer term, there is probably soem other uses...
      p('running kill games');
      games = null;
      p('games now null')
      delete games;  // i put this in bcus of reference to need for garbage collection - do not know if it is needed - lint is complaining about it
    }
    else {
      // p(game_master_ids);  // initial ids that exist
      gameControl(teamId, messageIn);
      return games;  // I do not think this return is used...
    }
  }

  // player messages
  else {
    if (typeof games !== "undefined") {
      gamePlay(teamId, messageIn);
      return games;  // I do not think this return is used...
    }
    else {
      var message = ('game server is not currently running');
      twil.sendSms(teamId, 'game server is not currently running', messageCallback);
    }
  }
}

/**
  * All the logic to control a game - start server, create game, start a game, sever stats, game stats, snapsot stats
*/
function gameControl(teamId, messageIn) {
  // parse incoming message into command and option
  var pattern = XRegExp('(?<command> ^\\w+ ) \\s  # grab the command \n\
              (?<option> .+ ) # grab everything else ', 'x');
  var match = XRegExp.exec(messageIn, pattern); // match should contain 2 named captures
  p(match);

  if (typeof games !== 'undefined') {

    // Game Server commands
    // test
    if (messageIn.toLowerCase() === 'test') {
      // nothing in here yet
      message = 'test function run';  // this doesn't currently do anything - should have function in Games
    }

    // game server stats
    else if (messageIn.toLowerCase() === 'stats') {
      games.stats();
    }

    // Game commands
    // create a game
    else if (match.command.toLowerCase() === 'create') {
      game = games.create(match.option, teamId);  //match.option is the game_id
    }

    // start a game
    else if (match.command.toLowerCase() === 'start') {
      games.active[match.option].start();
    }

    // game stats
    else if (match.command.toLowerCase() === 'stats') {
      games.active[match.option].stats();
    }

    // kill a game
    else if (match.command.toLowerCase() === 'killgame') {
      delete games.active[match.option];
    }

    // stats api
    else if (match.command.toLowerCase() === 'snapshot') {
      var snapshotJson = games.active[match.option].apiStatsSnapshot();
      p(JSON.stringify(snapshotJson));
    }

    else {
    message = 'messageIn did not match any options';  // message if nothing worked at all...
    }
  }
  else {
    if (messageIn.toLowerCase() === 'server') {
      games = new gserv.Games(teamId);
      // this sets game_master_id to be the first valid Id that starts the server... see github issue on this
      game_master_ids = null;
      game_master_ids = [teamId];  // I need to delete everything but the teamId... I could set it to null first... can prob use something like array.splice(0, array.length - 1, [the one you want to keep]) to do in one line
    }
    else {
      p('server has not been started');
    }
  }
}

/**
  * Logic to play a game (register, pick a name, answer question)
*/
function gamePlay(teamId, messageIn) {
  // check to see if teamId in a game
  teamIds = games.get_teamIds();  // this gets teamIds from all the games - checks to see if a team is in list
  if (typeof teamIds[teamId] !== "undefined") {
    var game = teamIds[teamId];  // get the game associated with the teamId
    // teamId is registered and game.stage is in play - so this an answer
    if (game.stage === 'play') {
      game.answer(teamId, messageIn);
    }
    // teamId is registered - game is still in register, ergo, this must be attempt to set team name
    else if (game.stage === 'register') {
      game.setName(teamId, messageIn, messageCallback);  // I don't think I need to send the messageCallback bcus it is defined in the game object
    }
    else {
      p('dude there are some problems - must check out');
    }
  }

  // check to see if game is in register mode (this is the else if so we can have nice error messaging... there might be a way to combine this with the else below)
  else if (typeof games.active[messageIn.toLowerCase()] !== "undefined" && games.active[messageIn.toLowerCase()].stage !== 'register') {
    twil.sendSms(teamId, 'game closed for registration', messageCallback);
  }

  // registration function (this will only run if team is not already registered)
  else {
    if (typeof games.active[messageIn.toLowerCase()] !== "undefined" && games.active[messageIn.toLowerCase()].stage === 'register') {  // check to see if there is a game that matches messageIn and the game is in register mode...
      games.active[messageIn.toLowerCase()].register(teamId);  // get the game object and register teamId
    }
    else {
      twil.sendSms(teamId, 'there is no game with that ID', messageCallback);    // hmmm - shld this actually live in gserv - prob  - this is in wrong place
    }
  }
}

exports.twilioProcess = twilioProcess;