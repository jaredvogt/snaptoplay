var http = require("http");
var url = require("url");
var utils = require('./utils');

utils.my_stuff();  // initialize my_stuff
p = console.log;

function start(route, handle) {
  function onRequest(request, response) {
    var pathname = url.parse(request.url).pathname;
    p("Request for {0} recieved.".format(pathname));
    route(handle, pathname, response, request);
  }

  http.createServer(onRequest).listen(8000);
  p("Server has started.");
}

exports.start = start;
