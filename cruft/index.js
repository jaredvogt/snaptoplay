var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var XRegExp = require('xregexp').XRegExp;
var test = require('./test');
var twil = require("./twilioSMS");

p = console.log;

var handle = {};
handle["/"] = requestHandlers.start;
handle["/twilio"] = requestHandlers.twilio;
handle["/gamestart"] = requestHandlers.gameStart;
handle["/gamecreate"] = requestHandlers.gameCreate;
handle["/gamestats"] = requestHandlers.gameStats;
handle["/gamesnapshot"] = requestHandlers.gameSnapshot;
handle["/test.html"] = requestHandlers.test_HTML;
//handle["/gamesnapshot/game0001"] = requestHandlers.gameSnapshotHC('game0001');
server.start(router.route, handle);


/// run in test mode
var pattern = XRegExp('(?<arg> ^\\w+ ) \\s? # grab the name  \n\
                    (?<more> .? ) # grab the message ', 'x');
// console.log(process.argv);
var match = XRegExp.exec(process.argv[2], pattern); // match should contain 2 named captures
if (match.arg == 'test') {
	test.prompt_start();
	var output = 'to console';
	// twil.output = 'to console';  // come back and use this
	p('running in test mode - redirect all messages to console/log');
	// test_output = ;
}
else if (match.arg == 'testsms') {
	test.prompt_start();
	var output = 'to sms';
	// twil.output = 'to console';
	p('running in test mode - all messages still go out via SMS');
}

exports.output = output;