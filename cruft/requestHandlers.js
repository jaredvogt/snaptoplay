var querystring = require("querystring"),
    fs = require("fs"),
    formidable = require("formidable"),
    twil = require("./twilioSMS"),
    XRegExp = require('xregexp').XRegExp,
    gserv = require("./gserv"),
    utils = require('./utils'),
    twilioHandler = require('./twilioHandler');

utils.my_stuff();  // initialize my_stuff
p = console.log;

/**
  * requestHandlers includes all of the handlers support by server.
*/
function twilio(response, request) {
  p("Request handler 'twilio' was called.");

  var form = new formidable.IncomingForm();
  form.parse(request, function(error, fields, files) {
    if (error) {
      p(error);
    }
    else {
      games = twilioHandler.twilio_process(fields.From, fields.Body, fields.SmsSid, fields.AccountSid);
    }
  });

  // this response goes back to twilio - I don't even know what heck do with it - they may ignore it completely
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("happiness");
  response.end();
}

// prob doesn't work
function gameCreate(response, request) {
    game = new gserv.Game(game_id, function (team_id, msg) {
      twil.send_sms(team_id, msg, function (response) {
        p(response);
      });
    });
    response.writeHead(200, {"Content-Type": "text/html"});
    //for (x in gameIds) {
    //response.write(gameIds[x]);
    //}
    response.write('create game with id: {0}'.format(game.id));
    response.end();
}

// prob doesn't work
function gameStart(response, request) {
    game.start();

    response.writeHead(200, {"Content-Type": "text/html"});
    //for (x in gameIds) {
    //response.write(gameIds[x]);
    //}
    response.write('game started');
    response.end();
}

// prob doesn't work
function gameStats(response, request) {
    // game.start();

    response.writeHead(200, {"Content-Type": "text/html"});
    response.write('game started');
    response.end();
}

// serve up json
function gameSnapshot(response, request) {
    // game.start();
    p('pingping');

    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(JSON.stringify(games.active['game0001'].api_stats_snapshot()));
    response.end();
}

function test_HTML(response, request) {
    // game.start();
    p('pingping');

    response.writeHead(200, {"Content-Type": "text/html"});
    // response.write(fs.readFile('test.html', function (err, data) {
    //   if (err) throw err;
    //   // console.log(data);
    //   return 'test'
    // }));
    response.write(function() {
      fs.readFile('./test.html', 'utf8', function (err, data) {
      if (err) throw err;
      console.log(data);
      });
    });
    // response.write('this is a test');
    response.end();
}

exports.twilio = twilio;
exports.gameStart = gameStart;
exports.gameCreate = gameCreate;
exports.gameStats = gameStats;
exports.gameSnapshot = gameSnapshot;
exports.test_HTML = test_HTML;

