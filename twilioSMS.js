var netrc = require('./node_modules/node-netrc/netrc');
var utils = require('./utils');
var configFrom = require('./.config').from;
var _ = require('underscore');

utils.myStuff();  // initialize myStuff
p = console.log;

var ACCOUNT_SID = netrc.host('twilio').login;
var AUTH_TOKEN = netrc.host('twilio').password;
var client = require('twilio')(ACCOUNT_SID, AUTH_TOKEN);
var from = configFrom;
var output = '';


/**
  * everything needed to send an sms message to twilio
*/
function sendSms(to, body, response) {
  // check to see if output should be redirect to console
  if (output === 'to console') {
    p('test redirect of outbound sms message: {0} {1}'.format(to, body));
  }
  else {
    client.sendSms({to: to, from: from, body: body}, function(err, sms_response) {
      var status = sms_response.status;
      var message = sms_response.message;
      var toNumber = sms_response.to;
      var sid = sms_response.sid;
      if (!err) {
        response('SUCCESS Number: {0}, status: {1}, message: {2}, sid: {3}'.format(toNumber, status, message, sid));
        // response(sms_response);
      }
      else {
        response('ERROR Status: {0}, Message: {1}'.format(status, message));
      }
    });
  }
}

function setOutput(destination) {
  output = destination;
}

exports.sendSms = sendSms;
exports.setOutput = setOutput;

