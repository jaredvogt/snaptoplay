var fs = require("fs"),
    XRegExp = require('xregexp').XRegExp,
    utils = require('./utils'),
    twilioHandler = require('./twilioHandler');

utils.myStuff();  // initialize myStuff
p = console.log;

// Define the route
var hello = {
    handler: function (request) {
        request.reply('hello world');
    }
};

// hander for incoming twilio messages
var twilio = {
    handler: function (request) {
        var fields = request.payload;
        games = twilioHandler.twilioProcess(fields.From, fields.Body, fields.SmsSid, fields.AccountSid);
        // console.log(request.payload)
        request.reply({ greeting: 'peachy' });
    }
};

// game dashboard handler
var dashboard = {
    handler: function (request) {
        // p(request.params.gameId);
        // check to see if this is a valid game
        var gameId = request.params.gameId;
        var apiOutput = 'game server is not running';
        p('dashboard handler called with game: {0}'.format(gameId));
        if (typeof games === 'undefined') {
            apiOutput = 'game server is not running';
            request.reply(apiOutput);
        }
        else if (typeof games.active[gameId] === 'undefined') {
            var allGames = '';
            for (var prop in games.active) {
                allGames = allGames + prop + ', ';
            }
            apiOutput = 'there is not game with that id - but these games do exist: {0}'.format(allGames);
            request.reply(apiOutput);
            // this would be a great place to grab another html template and send back links to all active games... actually - that page should exist elsewhere and just get called from this...
        }
        else {
            // p('dashboard - running the else');
            var template = fs.readFileSync('./public/dashboard_template.html', 'utf8');
            request.reply(template.format(gameId));  // insert the gameId in the template
        }
    }
};

/**
    * API calls
*/

// get snapshot off current round for a given game
var apiGame = {
    handler: function (request) {
        // add logic to not crash if game_id doesn't exit
        var gameId = request.params.gameId;
        var apiOutput = 'game server is not running';
        p('apiGame handler called');
        if (typeof games === 'undefined') {
            apiOutput = 'game server is not running';
            request.reply(apiOutput);
        }
        else if (typeof games.active[gameId] === 'undefined') {
            apiOutput = 'there is not game with that id';
            request.reply(apiOutput);
        }
        else if (games.active[gameId].stage == 'register') {
            apiOutput = 'game is not active';
            request.reply(apiOutput);
        }
        else {
            apiOutput = JSON.stringify(games.active[gameId].apiStatsSnapshot());
            if (typeof request.query['jsonp'] !== 'undefined') {  // this seems like original way
                p('running the if');
                request.reply("parseResponse({0})".format(apiOutput));
            }
            else if (typeof request.query['callback'] !== 'undefined') {  // this looks like a trick to use with jquery [could probably combine this one and the one above]
                p('running the else if');
                request.reply("{0}({1})".format(request.query['callback'], apiOutput));
            }
            else if (typeof request.query['allrounds'] !== 'undefined') {
                p('allrounds selected');  // intention of this line is to pass back data for all rounds - doesn't currently work with JSONP - this will have to split out to do that.
            }
            else {
                // request.reply(games.active[request.params.gameId].apiStatsSnapshot());
                request.reply(apiOutput);
            }
        }
    }
};


exports.hello = hello;
exports.twilio = twilio;
exports.apiGame = apiGame;
exports.dashboard = dashboard;