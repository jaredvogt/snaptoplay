# Welcome to Snaptoplay (aka semagrab or bargames)

To run this on a server, git clone the repo and run the following commands. To install all of the node libs that you need, run shell script.

```./support/install_modules.sh```  

To launch the server. 

```node snaptoplay.js```   

There are options.. the only one currently documented is ```node snaptoplay.js test```. This will launch the server in 'test' mode which allows you to batch in a bunch of test transactions. Once the server has started, typing ```man()``` at the prompt will show you those options (still to do).

Note: I have to fix SSL with twilio - so can't be run in https just yet.

### Little description of the included files

* ```snaptoplay.js``` main http server
* ```gserv.js``` game server
* ```hapiHandlers.js``` handler files for snaptoplay.js
* ```twilioHandler.js``` all the logic for parsing inbound twilio messages
* ```twilioSMS.js``` used to send outbound twilio messages
* ```utils.js``` couple of helper files
* ```test.js``` used for running tests

### More facts
```http://www.snaptoplay.com:8000/dashboard/[gameId]``` is the start of webpage to show a game dashboard. This loads ```public\dashboard_template.html``` - you can use the code in this file to call the API from a remote server. The API spits out JSONP - so it will work cross domain. You do need to replace the ```{0}``` in ```window.apiUrl``` with the gameId you want to hit. 
```http://www.snaptoplay.com:8000/dashboard/games``` will show you available games... (will format these into links at some point...). 

```public/``` has all the html/js/css files.



